package com.coffeeit.appium_example

interface FabListener {
    fun onFabClick()
}