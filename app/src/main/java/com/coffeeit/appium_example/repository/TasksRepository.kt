package com.coffeeit.appium_example.repository

import com.coffeeit.appium_example.model.Task

object TasksRepository {

    val tasks = mutableListOf<Task>()

    fun addTask(task: Task) {
        tasks.add(task)
    }

}