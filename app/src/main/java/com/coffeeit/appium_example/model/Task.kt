package com.coffeeit.appium_example.model

data class Task(
    val title: String,
    val description: String
)