package com.coffeeit.appium_example.fragment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.coffeeit.appium_example.model.Task
import com.coffeeit.appium_example.repository.TasksRepository

class TasksViewModel : ViewModel() {

    val tasks: LiveData<List<Task>>
        get() = _tasks

    private val _tasks: MutableLiveData<List<Task>> by lazy {
        MutableLiveData<List<Task>>(TasksRepository.tasks)
    }

    fun addTask(title: String, description: String) {
        TasksRepository.addTask(Task(title, description))
    }

}