package com.coffeeit.appium_example.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.coffeeit.appium_example.FabListener
import com.coffeeit.appium_example.R
import com.coffeeit.appium_example.databinding.AddTaskFragmentBinding
import com.google.android.material.snackbar.Snackbar

class AddTaskFragment : Fragment(), FabListener {

    private lateinit var binding: AddTaskFragmentBinding
    private val viewModel: TasksViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = AddTaskFragmentBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onFabClick() {
        val title = binding.title.text
        if (title.isNullOrEmpty()) {
            view?.let {
                Snackbar.make(it, "Please enter a title", Snackbar.LENGTH_LONG)
                    .setAction("Action", null)
                    .show()
            }
            return
        }
        val description = binding.description.text
        if (description.isNullOrEmpty()) {
            view?.let {
                Snackbar.make(it, "Please enter a description", Snackbar.LENGTH_LONG)
                    .setAction("Action", null)
                    .show()
            }
            return
        }
        viewModel.addTask(title.toString(), description.toString())

        findNavController().navigate(R.id.action_AddTaskFragment_to_TasksFragment)
    }

}