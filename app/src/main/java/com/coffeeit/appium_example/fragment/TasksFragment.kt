package com.coffeeit.appium_example.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.coffeeit.appium_example.FabListener
import com.coffeeit.appium_example.R
import com.coffeeit.appium_example.adapter.TasksAdapter
import com.coffeeit.appium_example.databinding.TasksFragmentBinding

class TasksFragment : Fragment(), FabListener {

    private lateinit var binding: TasksFragmentBinding
    private val viewModel: TasksViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = TasksFragmentBinding.inflate(inflater, container, false)
        initRecyclerView()
        return binding.root
    }

    private fun initRecyclerView() {
        val tasksAdapter = TasksAdapter()
        binding.tasks.adapter = tasksAdapter
        viewModel.tasks.value?.let {
            binding.emptyView.visibility = if (it.isEmpty()) View.VISIBLE else View.GONE
            tasksAdapter.submitList(it)
        }
    }

    override fun onFabClick() {
        findNavController().navigate(R.id.action_TasksFragment_to_AddTaskFragment)
    }

}